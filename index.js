/*jslint nomen:true, white:true */
/*globals require, module */
(function(){
    "use strict";
    var lib = require("./lib"),
    instance, validator;

    instance = (function () {
        function JavaScriptValidator() {
            this.debug = false;
            this.value = null;
            this.result = null;
            this.another = {};
            this.types = {};
            this._keys = {};
            this._set = function (o) {
                if (!lib.types.isEmptyObject(arguments) && !lib.types.isNull(o)) {
                    //this allows another but only if a current evaluation has been true
                    if ((this.value !== o && this.result)) {
                        this.result = undefined;
                    } else {
                        this.result = false;
                    }

                    this.value = o;
                }
            };
            this._validateResult = function () {
                if (this.result instanceof Error) {
                    this.error = this.result;
                    this.result = false;
                }
                return this;
            };
            //o.key represents name of function and o.validation represents validation function
            this.extend = function (o) {
                var getProp, context;
                this.another[o.key] = function (v) {
                    instance._set(v);
                    if ((typeof instance.result === "undefined") || instance.result) {
                        instance.result = instance.types[o.key].call(instance.types, instance.value);
                    }
                    return instance._validateResult();
                };

                //allow all validations internal access to all validations
                this.types[o.key] = o.validation;
                //add to keys for strongly typed comparisons 
                this._keys[o.key] = o.key;
                //context scope is within extend function
                context = {
                    value: o
                };

                if (lib.types.has.call(context, "hasSet") && (typeof o.hasSet === "boolean") && o.hasSet) {
                    instance[o.key] = function () {
                        //context scope within extension validation function wrapper 
                        var context = {
                            value: instance.value,
                            types: instance.types,
                            debug: instance.debug
                        };

                        if ((typeof instance.result === "undefined") || instance.result) {
                            instance.result = instance.types[o.key].apply(context, arguments);
                        }
                        return instance._validateResult();
                    };
                } else {
                    getProp = function () {
                        if ((typeof instance.result === "undefined") || instance.result) {
                            instance.result = instance.types[o.key].call(instance.types, instance.value);
                        }
                        return instance._validateResult();
                    };

                    Object.defineProperty(instance, o.key, {
                        get: getProp
                    });
                }
            };
            //reset this.value and this.result
            this.reset = function (o) {
                this.value = o;
                this.result = undefined;
            };

        }

        return new JavaScriptValidator();
    }());

    function isValidExtension(o) {
        var context, valid;

        context = {
            value: o
        };

        valid = lib.types.isObject(o) && lib.types.has.call(context, "key") && lib.types.has.call(context, "validation");
        valid = valid && lib.types.isNonEmptyString(o.key) && lib.types.isFunction(o.validation);

        context = {
            value: instance
        };

        valid = valid && !lib.types.has.call(context, o.key);
        return valid;
    }

    //o represents extension
    function onConfigure(o) {
        if (lib.types.isArray(o)) {
            o.map(function (item) {
                if (isValidExtension(item)) {
                    instance.extend(item);
                }
            });
        } else if (isValidExtension(o)) {
            instance.extend(o);
        }
    }

    validator = function (o, debug) {
        instance.debug = debug ? true : false;
        instance.reset(o);
        return instance;
    };
    validator.keys = instance._keys;

    validator.configure = onConfigure;
    validator.configure(lib.extensions);

    module.exports = validator;
}());