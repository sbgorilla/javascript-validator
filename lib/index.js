/*jslint nomen:true, node:true, white:true */
/*globals */
"use strict";
var types = require("./types");
var extensions = [];

Object.keys(types.keys).map(function (key) {
    var hasSet = (key === "has") || (key === "compare") || (key === "translate") || (key === "jsperf");

    extensions.push({
        key: key,
        validation: types[key],
        hasSet: hasSet
    });
});

exports.types = types;
exports.extensions = extensions;