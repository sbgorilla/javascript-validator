/*jslint nomen:true, white:true */
/*globals require, exports */
(function(){
    "use strict";
    var async = require("async"),
    fs = require("fs"),
    path = require("path");
    
    exports.run = function(options, keys, dir, next){
        var tasks; 
        
        tasks = [];
        
        keys.map(function(key){
            options[key].data = "";
            options[key].src.map(function(source){
                tasks.push(function(done){
                    var file = path.join(dir + "/" + source);
                
                    fs.readFile(file, "utf8", function(err, contents){
                        options[key].data = [options[key].data,contents].join(" ");
                        done();
                    });

                });
            });
            tasks.push(function(done){
                var file = path.join(dir + "/" + options[key].dest);
                
                fs.writeFile(file, options[key].data, "utf8", function(err){
                    done();
                });
            });
        });

        async.series(tasks, function(err){
           next();
        });
    };
}());