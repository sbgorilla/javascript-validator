/*jslint nomen:true, white:true */
/*globals console, exports */
(function(){
    "use strict";
    var types, compare, checkArray, comparison;

    function compareByType(value, definition) {
        if (types.isFunction(types[definition])) {
            return types[definition](value);
        } else {
            return false;
        }
    }

    function compareByObject(value, definition) {
        var vkeys, dkeys, vprop, dprop, req, rprop, error, context;

        context = {
            value: definition
        };

        vkeys = Object.keys(value);

        if (types.has.call(context, "required") && types.isArray(definition.required)) {
            req = definition.required;
        }

        if (types.has.call(context, "props") && types.isObject(definition.props) && !(types.isArray(definition.props))) {
            dkeys = Object.keys(definition.props);
        }

        if (types.isArray(vkeys) && types.isArray(dkeys)) {
            if (types.isArray(req)) {
                req.map(function (key) {
                    if (vkeys.indexOf(key) === -1) {
                        throw new Error("required property {" + key + "} missing");
                    }
                });
            }

            dkeys.map(function (key) {
                vprop = value[key];
                dprop = definition.props[key];

                if (!req) {
                    //default to definition properties match value properties
                    rprop = vkeys.indexOf(key) !== -1;
                } else {
                    rprop = req.indexOf(key) !== -1;
                }

                if (!vprop || (vprop && types.isString(dprop) && (!compareByType(vprop, dprop) || !rprop))) {
                    throw new Error(key + ": {value:" + vprop + "} {expected: " + definition.props[key] + "}");
                } else if ((vprop && !types.isNull(dprop) && types.isObject(dprop) && !types.isString(dprop))) {
                    compareByObject(vprop, dprop);
                }
            });
        } else {
            throw new Error("defintion or comparison object invalid");
        }

        return !error;
    }

    compare = function (value, definition, callback, debug) {
        var valid, error, verified_value, verified_definition, single_prop, context;

        context = {
            value: types
        };

        verified_value = !types.isNull(value) && types.isObject(value);
        verified_definition = !types.isNull(definition) && types.isObject(definition);
        single_prop = types.isNonEmptyString(definition) && types.has.call(context, definition);

        if (verified_value && !types.isArray(value) && verified_definition) {
            try {
                valid = compareByObject(value, definition);
            } catch (objErr) {
                if(debug){ 
                    types.log(objErr.stack);
                }
                error = objErr;
            }
        } else if (verified_value && types.isArray(value) && (verified_definition || (single_prop && definition !== "array"))) {
            try {
                valid = checkArray(value, definition);
            } catch (arrayErr) {
                if(debug){
                    types.log(arrayErr.stack);
                }
                error = arrayErr;
            }
        } else if (single_prop) {
            valid = compareByType(value, definition);
        }

        if (types.isFunction(callback)) {
            callback(error, valid);
        } else {
            //errors are not thrown or reported when callback is undefined
            if (error) {
                valid = false;
            }

            return valid;
        }
    };

    checkArray = function (collection, definition) {
        var error;

        if (types.isArray(collection) && (types.isObject(definition) || types.isString(definition))) {
            collection.map(function (value, index) {
                if (!compare(value, definition)) {
                    throw new Error("array index " + index + ": {value:" + value + "} {expected: " + definition + "}");
                }
            });
        } else {
            throw new Error("invalid collection or definition");
        }

        return !error;
    };

    function translate(value, definition) {
        var result, prop;

        result = {};

        function getObjectValue(v, d) {
            var props, temp, i;

            if (types.isString(d)) {
                props = d.split(".");

                if (types.isArray(props)) {
                    temp = v;
                    props.map(function (key) {
                        var context = {
                            value: temp
                        };

                        if (types.has.call(context, key)) {
                            temp = temp[key];
                        } else {
                            temp = new Error("could not find " + key + " during translation");
                            return;
                        }
                    });
                }
            } else {
                temp = new Error("could not parse options identifier");
            }

            return temp;
        }

        Object.keys(definition).map(function (key) {
            prop = getObjectValue(value, definition[key]);
            if (prop instanceof Error) {
                result = prop;
                return;
            } else {
                result[key] = prop;
            }
        });

        return result;
    }

    comparison = (function () {
        return {
            compare: function (o, n) {
                types = this.types;
                return compare(this.value, o, n, this.debug);
            },
            translate: function (o) {
                types = this.types;
                return translate(this.value, o, this.debug);
            }
        };
    }());

    Object.keys(comparison).map(function (key) {
        exports[key] = comparison[key];
    });
}());