/*jslint nomen:true, white:true */
/*globals console, exports */
(function(){
    "use strict";
    var objects = (function () {
        return {
            isNull: function (o) {
                return (typeof o === "undefined") || (typeof o === "object" && o === null);
            },
            isObject: function (o) {
                return (typeof o === "object") && (o !== null);
            },
            isEmptyObject: function (o) {
                return ((typeof o === "object") && (Object.keys(o).length === 0));
            },
            isArray: function (o) {
                return !objects.isNull(o) && o instanceof Array;
            },
            isDate: function (o) {
                return !objects.isNull(o) && o instanceof Date;
            },
            isString: function (o) {
                return !objects.isNull(o) && (o instanceof String || typeof o === "string");
            },
            isNumber: function (o) {
                return !objects.isNull(o) && (typeof o === "number" || o instanceof Number) && !isNaN(o);
            },
            isInt: function (o) {
                return objects.isNumber(o) && o.toString().indexOf(".") === -1;
            },
            isFunction: function (o) {
                return (typeof o === "function") && (o instanceof Function);
            },
            has: function (o) {
                return !objects.isNull(this.value) && (this.value.hasOwnProperty(o) || !objects.isNull(this.value[o]));
            },
            log: function (o) {
                if ((typeof console !== "undefined") && (console.log instanceof Function)) {
                    console.log(o);
                }
            }
        };
    }());

    Object.keys(objects).map(function (key) {
        exports[key] = objects[key];
    });
}());