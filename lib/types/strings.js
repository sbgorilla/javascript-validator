/*jslint nomen:true, white:true */
/*globals exports */
(function(){
    "use strict";
    var strings = (function () {
        return {
            _whitespace: new RegExp(/\s/g),
            isNullOrEmpty: function (o) {
                if (typeof o === "string") {
                    return (o.replace(strings._whitespace, "") === "");
                } else {
                    return true;
                }
            },
            isNonEmptyString: function (o) {
                return (typeof o === "string") && !strings.isNullOrEmpty(o);
            },
            isNumericString: function (o) {
                return strings.isNonEmptyString(o) && !isNaN(o);
            },
            isDateString: function (o) {
                try {
                    return strings.isNonEmptyString(o) && !isNaN(Date.parse(o));
                } catch (dateErr) {
                    return false;
                }
            }
        };
    }());

    Object.keys(strings).map(function (key) {
        if (key !== "_whitespace") {
            exports[key] = strings[key];
        }
    });
}());