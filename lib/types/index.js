/*jslint nomen:true, white:true */
/*globals require, exports */
(function(){
    "use strict";
    var modules, keys;

    modules = [];
    keys = {};

    modules.push(require("./objects"));
    modules.push(require("./strings"));
    modules.push(require("./comparison"));
    modules.push(require("./jsperf"));

    modules.map(function (module) {
        Object.keys(module).map(function (key) {
            exports[key] = module[key];
            keys[key] = key;
        });
    });

    exports.keys = keys;
}());