/*jslint white:true, nomen:true */
/*globals performance, exports, console */
(function () {
    "use strict";
    
    Function.prototype.jsperf = function(args, print){ 
        var start, end, value;
        
        function now(){ 
            if(typeof performance !== "undefined"){ 
                return performance.now();   
            }
            else { 
                return Date.now();
            }
        }
        
        function getExecutionTime(o){ 
            var minutes, seconds, miliseconds, data; 
             
            miliseconds = o % 1000;
            seconds = Math.floor(o / 1000);
            minutes = Math.floor(seconds / 60); 
            seconds = seconds % 60;
            
            data = [
                minutes,"\t","minutes\n",
                seconds,"\t","seconds\n",
                miliseconds,"\t","miliseconds"
            ];
            
            return data.join("");
        }
        
        start = now();
        value = this.apply(this, args);
        end = now(); 
        
        if(print){ 
            console.log(["/**","jsperf for " + this.name, "start","**/"].join("\t"));
            console.log("total execution:");
            console.log(getExecutionTime((end - start))); 
            console.log(this.toString());
            console.log(["/**","jsperf for " + this.name, "end","**/"].join("\t"));
        }
         
        return value;
    };



    exports.jsperf = function(args, print){ 
        return this.value.jsperf(args, print);
    };
}());