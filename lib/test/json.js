/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var validator = require("../../index");

describe("comparing json with definitions", function () {
    it("object single property of type string", function (done) {
        var obj, definition;

        obj = {
            a_prop: "some text",
            b_prop: "other text"
        };

        definition = {
            props: {
                a_prop: validator.keys.isNonEmptyString
            },
            required: [
                "a_prop"
            ]
        };

        validator(obj).compare(definition, function (err, result) {
            assert(!err, "should not error on checking for non null string");
            assert(result, "result should return true");
            done();
        });
    });
    it("object single property of type string avoiding required", function (done) {
        var obj, definition;

        obj = {
            a_prop: "some text"
        };

        definition = {
            props: {
                a_prop: validator.isNonEmptyString
            }
        };

        validator(obj).compare(definition, function (err, result) {
            assert(!err, "should not error on checking for non null string");
            assert(result, "result should return true");
            done();
        });
    });
    it("object checking of properties that are defined as object", function (done) {
        var obj, definition;

        obj = {
            a_prop: {
                a_sub_prop: "some text",
                b_sub_prop: ""
            },
            b_prop: new Date(),
            c_prop: 4,
            d_prop: 4.4,
            e_prop: []
        };

        definition = {
            props: {
                a_prop: {
                    props: {
                        a_sub_prop: validator.isNonEmptyString
                    }
                },
                b_prop: validator.keys.isDate,
                c_prop: validator.keys.isInt,
                d_prop: validator.keys.isNumber,
                e_prop: validator.keys.isArray
            }
        };

        validator(obj).compare(definition, function (err, result) {
            assert(!err, "should not error on checking for non null string");
            assert(result, "result should return true");
            done();
        });
    });
});