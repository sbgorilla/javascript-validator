/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var validator = require("../../index");

describe("checking arrays: ", function () {
    it("array of string", function (done) {
        var array, definition;

        array = [
            "some text",
            "some other text"
        ];

        definition = validator.keys.isString;

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error on checking for an array of string");
            assert(result, "result should return true for array of string");
            done();
        });
    });
    it("array of number", function (done) {
        var array, definition;

        array = [
            4.5,
            5
        ];

        definition = validator.keys.isNumber;

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "result should return true for array of number");
            done();
        });
    });
    it("array of int", function (done) {
        var array, definition;

        array = [
            5,
            5
        ];

        definition = validator.keys.isInt;

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "result should return true for array of int");
            done();
        });
    });
    it("array of date", function (done) {
        var array, definition;

        array = [
            new Date()
        ];

        definition = validator.keys.isDate;

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "result should return true for array of date");
            done();
        });
    });
    it("array of objects", function (done) {
        var array, definition;

        array = [{
            a_prop: "some text"
        }];

        definition = {
            props: {
                a_prop: validator.keys.isNonEmptyString
            }
        };

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "result should return true for array of object");
            done();
        });
    });
    it("array of objects sub object inspect", function (done) {
        var array, definition;

        array = [{
            a_prop: "some text",
            b_prop: {
                a_sub_prop: new Date()
            }

        }];

        definition = {
            props: {
                a_prop: validator.keys.isNonEmptyString,
                b_prop: {
                    props: {
                        a_sub_prop: validator.keys.isDate
                    }
                }
            }
        };

        validator(array).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "result should return true for array of object");
            done();
        });
    });
});