/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var validator = require("../../index.js");

describe("simple extension test: ", function () {
    it("simple custom text validation", function (done) {
        var extension = {
            key: "dayOfTheWeek",
            validation: function (o) {
                var valid, days = ["mon", "tue", "wed", "thu", "fri"];

                days.map(function (key, i) {
                    if (key === days[i]) {
                        valid = true;
                        return;
                    }
                });

                return valid;
            }

        };

        validator.configure([extension]);
        assert(validator("tue").dayOfTheWeek.result, "should be a day of the week");
        done();
    });
});