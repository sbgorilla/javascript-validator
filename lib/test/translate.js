/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var validator = require("../../index");

describe("object translation", function () {
    it("remap an object literal", function (done) {
        var obj, definition, result, compareto;

        obj = {
            user: {
                last_name: "davis",
                first_name: "ted",
                home_address: {
                    street: "1000 some street",
                    city: "some city",
                    state: "somewhere",
                    zip: 55555
                },
                date_of_birth: new Date()
            }
        };

        definition = {
            "ln": "user.last_name",
            "fn": "user.first_name",
            "zip": "user.home_address.zip",
            "state": "user.home_address.state",
            "dob": "user.date_of_birth"
        };

        compareto = {
            props: {
                ln: validator.keys.isNonEmptyString,
                fn: validator.keys.isNonEmptyString,
                zip: validator.keys.isInt,
                state: validator.keys.isNonEmptyString,
                dob: validator.keys.isDate
            }
        };

        result = validator(obj).translate(definition).result;

        assert(result !== null && (typeof result === "object"), "should translate object");

        validator(result).compare(compareto, function (err, result) {
            assert(!err, "should not error comparing two objects");
            assert(result, "should have validated the translated object");
            done();
        });
    });
});