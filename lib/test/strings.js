/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var strings = require("../types/strings");

describe("some default string positive tests", function () {
    it("string null", function (done) {
        assert(strings.isNullOrEmpty(null), "string should be null");
        done();
    });
    it("string empty", function (done) {
        assert(strings.isNullOrEmpty(""), "string should be empty");
        done();
    });
    it("string not empty", function (done) {
        assert(!strings.isNullOrEmpty("a"), "string should not be empty");
        done();
    });
    it("string non empty", function (done) {
        assert(strings.isNonEmptyString("a"), "string should not be empty");
        done();
    });
    it("string numeric", function (done) {
        assert(strings.isNumericString("44"), "string should be numeric");
        done();
    });
    it("string not numberic", function (done) {
        assert(!strings.isNumericString("bb"), "string should be not numeric");
        done();
    });
    it("string date", function (done) {
        assert(strings.isDateString(new Date().toDateString()), "string should be date");
        done();
    });
    it("string not date", function (done) {
        assert(!strings.isDateString("asdf"), "string should not be date");
        done();
    });
});