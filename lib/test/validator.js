/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var validator = require("../../index");

describe("test the wrapper", function () {
    it("method chain compare", function (done) {
        var obj, definition;

        obj = {
            a_prop: "some text",
            b_prop: "other text"
        };


        definition = {
            props: {
                a_prop: validator.keys.isNonEmptyString
            },
            required: ["a_prop"]
        };


        validator(obj).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "should return true");
            done();
        });
    });
    it("method chain compare array", function (done) {
        var obj, definition;

        obj = [
            "some text",
            "adfd"
        ];

        definition = validator.keys.isNonEmptyString;

        validator(obj).compare(definition, function (err, result) {
            assert(!err, "should not error");
            assert(result, "should return true");
            done();
        });
    });
    it("method chain check if object has properties", function (done) {
        var obj, result;

        obj = {
            age: 5,
            height: 5.8,
            weight: 190
        };

        result = validator(obj).has("age").has("height").has("weight").result;
        assert(result, "should be true");
        done();
    });
    it("method chain false persists through chain", function (done) {
        var obj, result;

        obj = {
            age: 5,
            height: 5.8,
            weight: 190
        };

        result = validator(obj).has("age").has("height").has("none").has("weight").result;
        assert(!result, "should be true");
        done();
    });
    it("remap an object literal", function (done) {
        var obj, definition, result, compareto, options;

        obj = {
            user: {
                last_name: "davis",
                first_name: "ted",
                home_address: {
                    street: "1000 some street",
                    city: "some city",
                    state: "somewhere",
                    zip: 55555
                },
                date_of_birth: new Date()
            }
        };

        definition = {
            "ln": "user.last_name",
            "fn": "user.first_name",
            "zip": "user.home_address.zip",
            "state": "user.home_address.state",
            "dob": "user.date_of_birth"
        };

        compareto = {
            props: {
                ln: validator.keys.isNonEmptyString,
                fn: validator.keys.isNonEmptyString,
                zip: validator.keys.isInt,
                state: validator.keys.isNonEmptyString,
                dob: validator.keys.isDate
            }
        };

        result = validator(obj).translate(definition).result;

        validator(result).compare(compareto, function (err, result) {
            assert(!err, "should not erro comparing two objects");
            assert(result, "should have validated the translated object");
            done();
        });
    });
    it("method chain combining lib function", function (done) {
        var result;

        result = validator("5/2/12").isString.isNonEmptyString.isDateString.result;
        assert(result, "should be true");
        done();
    });
    it("method chain combining lib function mixed variables", function (done) {
        var obj, result;

        obj = {
            age: 5,
            height: 5.8,
            weight: 190
        };

        result = validator(obj).isObject.has("age").another.isString("5/2/12").isNonEmptyString.isDateString.result;
        assert(result, "should be true");
        done();
    });
    it("method chain combining test reset non false positive", function (done) {
        var obj, result;
        //sets internal value once
        result = validator("asdfasd").isNumber.result;
        //test provides if this clears correctly
        assert(!validator(obj).isNonEmptyString.result, "should be true");
        done();
    });
});