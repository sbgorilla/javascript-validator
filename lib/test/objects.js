/*jslint nomen:true, node:true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var objects = require("../types/objects");

describe("some default object positive tests", function () {
    it("object null", function (done) {
        assert(objects.isNull(null), "object should be null");
        done();
    });
    it("object not null", function (done) {
        assert(!objects.isNull({}), "object should not be null");
        done();
    });
    it("object not array", function (done) {
        assert(!objects.isArray(null), "object should not be an array");
        done();
    });
    it("object is array", function (done) {
        assert(objects.isArray([]), "object should be an array");
        done();
    });
    it("object is date", function (done) {
        assert(objects.isDate(new Date()), "object should be an date");
        done();
    });
    it("object not is date", function (done) {
        assert(!objects.isDate(4), "object should not be an date");
        done();
    });
    it("object is string", function (done) {
        assert(objects.isString(""), "object should be an string");
        done();
    });
    it("object not is string", function (done) {
        assert(!objects.isString({}), "object should not be an string");
        done();
    });
    it("object is number", function (done) {
        assert(objects.isNumber(4.4), "object should be an number");
        done();
    });
    it("object not is number", function (done) {
        assert(!objects.isNumber("2323"), "object should not be an number");
        done();
    });
    it("object is int", function (done) {
        assert(objects.isInt(4), "object should be an int");
        done();
    });
    it("object not is int", function (done) {
        assert(!objects.isInt(4.4), "object should not be an int");
        done();
    });
    it("object is function", function (done) {
        assert(objects.isFunction(function () {}), "object should be an function");
        done();
    });
    it("object not is function", function (done) {
        assert(!objects.isFunction("alksdjfasd"), "object should not be an function");
        done();
    });
    it("object has property prop", function (done) {
        var context = {
            value: {
                prop: ""
            }
        };
        assert(objects.has.call(context, "prop"), "object should have property prop");
        done();
    });
});