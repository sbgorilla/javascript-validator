/*jslint nomen:true, white:true */
/*globals describe, it, require */
(function(){
    "use strict";
    var assert = require("assert"),
    validator = require("../../index");
    

    describe("jspeft tests", function () {
        it("number generate function test", function (done) {
            var value; 
            
            function numGenerate(limit){ 
                var i, a = 0;
                for(i = 0; i < limit; i = i + 1){ 
                    a = a + 1;
                }
                return a; 
            }
            
            //no need to see console print out of function data
            value = validator(numGenerate).jsperf([4000000000], false).result;
            assert(value > 0, "value must have value");
            done();
        });

    });
}());