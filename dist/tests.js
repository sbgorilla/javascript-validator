/*jslint nomen:true, white:true */
/*globals document, jv */

var element = document.getElementById("test-results");
var fragment = document.createDocumentFragment();
var list = document.createElement("ul");
var li;

function assert(valid, message) {
    "use strict";
    li = document.createElement("li");
    if (typeof valid === "string") {
        li.innerHTML = valid;
    } else {
        li.innerHTML = "result: [" + valid + "] message: " + message;
    }
    list.appendChild(li);
}

assert("<b>String Tests</b>");
assert(jv(null).isNullOrEmpty.result, "string should be null");
assert(jv("").isNullOrEmpty.result, "string should be empty");
assert(!jv("test").isNullOrEmpty.result, "string should not be empty");
assert(jv("sample").isNonEmptyString.result, "string should not be empty");
assert(jv("44").isNumericString.result, "string should be numeric");
assert(!jv("notanumber").isNumericString.result, "string should be not numeric");
assert(jv(new Date().toDateString()).isDateString.result, "string should be date");
assert(!jv("somedatestring").isDateString.result, "string should not be date");

assert("<b>Object Tests</b>");
assert(jv(null).isNull.result, "object should be null");
assert(!jv({}).isNull.result, "object should not be null");
assert(!jv(null).isArray.result, "object should not be an array");
assert(jv([]).isArray.result, "object should be an array");
assert(jv(new Date()).isDate.result, "object should be an date");
assert(!jv(4).isDate.result, "object should not be an date");
assert(jv("").isString.result, "object should be an string");
assert(!jv({}).isString.result, "object should not be an string");
assert(jv(4.4).isNumber.result, "object should be an number");
assert(!jv("2323").isNumber.result, "object should not be an number");
assert(jv(4).isInt.result, "object should be an int");
assert(!jv(4.4).isInt.result, "object should not be an int");
assert(jv(function () {
    "use strict";
}).isFunction.result, "object should be an function");
assert(!jv("alksdjfasd").isFunction.result, "object should not be an function");
//has at this point is only for method chaining hence comment out
//assert(jv.has({prop: ""}, "prop").result, "object should have property prop");

assert("<b>Array Tests</b>");
var array, definition;

array = [
    "some text",
    "some other text"
];

definition = jv.keys.isString;
assert(jv(array).compare(definition).result, "result should return true for array of string");

array = [5, 5];
definition = jv.keys.isInt;
assert(jv(array).compare(definition).result, "result should return true for array of int");

array = [new Date()];

definition = jv.keys.isDate;
assert(jv(array).compare(definition).result, "result should return true for array of date");

array = [{
    a_prop: "some text"
}];

definition = {
    props: {
        a_prop: jv.keys.isNonEmptyString
    }
};
assert(jv(array).compare(definition).result, "result should return true for array of object");

array = [{
    a_prop: "some text",
    b_prop: {
        a_sub_prop: new Date()
    }
}];

definition = {
    props: {
        a_prop: jv.keys.isNonEmptyString,
        b_prop: {
            props: {
                a_sub_prop: jv.keys.isDate
            }
        }
    }
};
assert(jv(array).compare(definition).result, "result should return true for array of object");

assert("<b>Object Tests</b>");
var obj;

obj = {
    a_prop: "some text",
    b_prop: "other text"
};

definition = {
    props: {
        a_prop: jv.keys.non_empty_string
    },
    required: [
        "a_prop"
    ]
};
assert(jv(obj).compare(definition).result, "result should return true when comparing objects");

obj = {
    a_prop: "some text"
};

definition = {
    props: {
        a_prop: jv.keys.isNonEmptyString
    }
};
assert(jv(obj).compare(definition).result, "result should return true when comparing objects");

obj = {
    a_prop: {
        a_sub_prop: "some text",
        b_sub_prop: ""
    },
    b_prop: new Date(),
    c_prop: 4,
    d_prop: 4.4,
    e_prop: []
};

definition = {
    props: {
        a_prop: {
            props: {
                a_sub_prop: jv.keys.isNonEmptyString
            }
        },
        b_prop: jv.keys.isDate,
        c_prop: jv.keys.isInt,
        d_prop: jv.keys.isNumber,
        e_prop: jv.keys.isArray
    }
};
assert(jv(obj).compare(definition).result, "result should return true when comparing objects");

assert("<b>Translation  Tests</b>");
var result, compareto;

obj = {
    user: {
        last_name: "davis",
        first_name: "ted",
        home_address: {
            street: "1000 some street",
            city: "some city",
            state: "somewhere",
            zip: 55555
        },
        date_of_birth: new Date()
    }
};

definition = {
    "ln": "user.last_name",
    "fn": "user.first_name",
    "zip": "user.home_address.zip",
    "state": "user.home_address.state",
    "dob": "user.date_of_birth"
};

compareto = {
    props: {
        ln: jv.keys.isNonEmptyString,
        fn: jv.keys.isNonEmptyString,
        zip: jv.keys.isInt,
        state: jv.keys.isNonEmptyString,
        dob: jv.keys.isDate
    }
};

result = jv(obj).translate(definition).result;
assert(jv(result).compare(compareto).result, "should have validated the translated object");

assert("<b>Validator Tests(function call and chaining tests)</b>");

obj = {
    a_prop: "some text",
    b_prop: "other text"
};

definition = {
    props: {
        a_prop: jv.keys.isNonEmptyString
    },
    required: [
        "a_prop"
    ]
};

assert(jv(obj).compare(definition).result, "result should return true when comparing objects");

obj = [
    "some text",
    "adfd"
];

definition = jv.keys.isNonEmptyString;
assert(jv(obj).compare(definition).result, "result should return true when comparing objects");

obj = {
    age: 5,
    height: 5.8,
    weight: 190
};

result = jv(obj).has("age").has("height").has("weight").result;
assert(result, "should be true");

obj = {
    age: 5,
    height: 5.8,
    weight: 190
};

result = jv(obj).has("age").has("height").has("none").has("weight").result;
assert(!result, "should be true");


obj = {
    user: {
        last_name: "davis",
        first_name: "ted",
        home_address: {
            street: "1000 some street",
            city: "some city",
            state: "somewhere",
            zip: 55555
        },
        date_of_birth: new Date()
    }
};

definition = {
    "ln": "user.last_name",
    "fn": "user.first_name",
    "zip": "user.home_address.zip",
    "state": "user.home_address.state",
    "dob": "user.date_of_birth"
};

compareto = {
    props: {
        ln: jv.keys.isNonEmptyString,
        fn: jv.keys.isNonEmptyString,
        zip: jv.keys.isInt,
        state: jv.keys.isNonEmptyString,
        dob: jv.keys.isDate
    }
};

result = jv(obj).translate(definition).result;
assert(jv(result).compare(compareto).result, "should have validated the translated object");


result = jv("5/2/12").isString.isNonEmptyString.isDateString.result;
assert(result, "should be true");


obj = {
    age: 5,
    height: 5.8,
    weight: 190
};

result = jv(obj).isObject.has("age").another.isString("5/2/12").isNonEmptyString.isDateString.result;
assert(result, "should be true");

//sets internal value once
result = jv("asdfasd").isNumber.result;
//test provides if this clears correctly
assert(!jv(obj).isNonEmptyString.result, "should be true");

fragment.appendChild(list);
element.appendChild(fragment);