  (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*jslint nomen:true, node: true, white:true */
/*globals jv:true */

jv = require("./index.js");
},{"./index.js":2}],2:[function(require,module,exports){
/*jslint nomen:true, white:true, node:true*/
/*globals */
"use strict";
var lib = require("./lib");

var instance, validator;

instance = (function () {
    function JavaScriptValidator() {
        this.debug = false;
        this.value = null;
        this.result = null;
        this.another = {};
        this.types = {};
        this._keys = {};
        this._set = function (o) {
            if (!lib.types.isEmptyObject(arguments) && !lib.types.isNull(o)) {
                //this allows another but only if a current evaluation has been true
                if ((this.value !== o && this.result)) {
                    this.result = undefined;
                } else {
                    this.result = false;
                }

                this.value = o;
            }
        };
        this._validateResult = function () {
            if (this.result instanceof Error) {
                this.error = this.result;
                this.result = false;
            }
            return this;
        };
        //o.key represents name of function and o.validation represents validation function
        this.extend = function (o) {
            var getProp, context;
            this.another[o.key] = function (v) {
                instance._set(v);
                if ((typeof instance.result === "undefined") || instance.result) {
                    instance.result = instance.types[o.key].call(instance.types, instance.value);
                }
                return instance._validateResult();
            };

            //allow all validations internal access to all validations
            this.types[o.key] = o.validation;
            //add to keys for strongly typed comparisons 
            this._keys[o.key] = o.key;
            //context scope is within extend function
            context = {
                value: o
            };

            if (lib.types.has.call(context, "hasSet") && (typeof o.hasSet === "boolean") && o.hasSet) {
                instance[o.key] = function () {
                    //context scope within extension validation function wrapper 
                    var context = {
                        value: instance.value,
                        types: instance.types,
                        debug: instance.debug
                    };

                    if ((typeof instance.result === "undefined") || instance.result) {
                        instance.result = instance.types[o.key].apply(context, arguments);
                    }
                    return instance._validateResult();
                };
            } else {
                getProp = function () {
                    if ((typeof instance.result === "undefined") || instance.result) {
                        instance.result = instance.types[o.key].call(instance.types, instance.value);
                    }
                    return instance._validateResult();
                };

                Object.defineProperty(instance, o.key, {
                    get: getProp
                });
            }
        };
        //reset this.value and this.result
        this.reset = function (o) {
            this.value = o;
            this.result = undefined;
        };

    }

    return new JavaScriptValidator();
}());

function isValidExtension(o) {
    var context, valid;

    context = {
        value: o
    };

    valid = lib.types.isObject(o) && lib.types.has.call(context, "key") && lib.types.has.call(context, "validation");
    valid = valid && lib.types.isNonEmptyString(o.key) && lib.types.isFunction(o.validation);

    context = {
        value: instance
    };

    valid = valid && !lib.types.has.call(context, o.key);
    return valid;
}

//o represents extension
function onConfigure(o) {
    if (lib.types.isArray(o)) {
        o.map(function (item) {
            if (isValidExtension(item)) {
                instance.extend(item);
            }
        });
    } else if (isValidExtension(o)) {
        instance.extend(o);
    }
}

validator = function (o, debug) {
    instance.debug = debug ? true : false;
    instance.reset(o);
    return instance;
};
validator.keys = instance._keys;

validator.configure = onConfigure;
validator.configure(lib.extensions);

module.exports = validator;
},{"./lib":3}],3:[function(require,module,exports){
/*jslint nomen:true, node:true, white:true */
/*globals */
"use strict";
var types = require("./types");
var extensions = [];

Object.keys(types.keys).map(function (key) {
    var hasSet = (key === "has") || (key === "compare") || (key === "translate");

    extensions.push({
        key: key,
        validation: types[key],
        hasSet: hasSet
    });
});

exports.types = types;
exports.extensions = extensions;
},{"./types":5}],4:[function(require,module,exports){
/*jslint nomen:true, white:true */
/*globals exports, console */
var comparison = (function () {
    "use strict";
    var types, compare, checkArray;

    function compareByType(value, definition) {
        if (types.isFunction(types[definition])) {
            return types[definition](value);
        } else {
            return false;
        }
    }

    function compareByObject(value, definition) {
        var vkeys, dkeys, vprop, dprop, req, rprop, error, context;

        context = {
            value: definition
        };

        vkeys = Object.keys(value);

        if (types.has.call(context, "required") && types.isArray(definition.required)) {
            req = definition.required;
        }

        if (types.has.call(context, "props") && types.isObject(definition.props) && !(types.isArray(definition.props))) {
            dkeys = Object.keys(definition.props);
        }

        if (types.isArray(vkeys) && types.isArray(dkeys)) {
            if (types.isArray(req)) {
                req.map(function (key) {
                    if (vkeys.indexOf(key) === -1) {
                        throw new Error("required property {" + key + "} missing");
                    }
                });
            }

            dkeys.map(function (key) {
                vprop = value[key];
                dprop = definition.props[key];

                if (!req) {
                    //default to definition properties match value properties
                    rprop = vkeys.indexOf(key) !== -1;
                } else {
                    rprop = req.indexOf(key) !== -1;
                }

                if (!vprop || (vprop && types.isString(dprop) && (!compareByType(vprop, dprop) || !rprop))) {
                    throw new Error(key + ": {value:" + vprop + "} {expected: " + definition.props[key] + "}");
                } else if ((vprop && !types.isNull(dprop) && types.isObject(dprop) && !types.isString(dprop))) {
                    compareByObject(vprop, dprop);
                }
            });
        } else {
            throw new Error("defintion or comparison object invalid");
        }

        return !error;
    }

    compare = function (value, definition, callback, debug) {
        var valid, error, verified_value, verified_definition, single_prop, context;

        context = {
            value: types
        };

        verified_value = !types.isNull(value) && types.isObject(value);
        verified_definition = !types.isNull(definition) && types.isObject(definition);
        single_prop = types.isNonEmptyString(definition) && types.has.call(context, definition);

        if (verified_value && !types.isArray(value) && verified_definition) {
            try {
                valid = compareByObject(value, definition);
            } catch (objErr) {
                if(debug){ 
                    types.log(objErr.stack);
                }
                error = objErr;
            }
        } else if (verified_value && types.isArray(value) && (verified_definition || (single_prop && definition !== "array"))) {
            try {
                valid = checkArray(value, definition);
            } catch (arrayErr) {
                if(debug){
                    types.log(arrayErr.stack);
                }
                error = arrayErr;
            }
        } else if (single_prop) {
            valid = compareByType(value, definition);
        }

        if (types.isFunction(callback)) {
            callback(error, valid);
        } else {
            //errors are not thrown or reported when callback is undefined
            if (error) {
                valid = false;
            }

            return valid;
        }
    };

    checkArray = function (collection, definition) {
        var error;

        if (types.isArray(collection) && (types.isObject(definition) || types.isString(definition))) {
            collection.map(function (value, index) {
                if (!compare(value, definition)) {
                    throw new Error("array index " + index + ": {value:" + value + "} {expected: " + definition + "}");
                }
            });
        } else {
            throw new Error("invalid collection or definition");
        }

        return !error;
    };

    function translate(value, definition) {
        var result, prop;

        result = {};

        function getObjectValue(v, d) {
            var props, temp, i;

            if (types.isString(d)) {
                props = d.split(".");

                if (types.isArray(props)) {
                    temp = v;
                    props.map(function (key) {
                        var context = {
                            value: temp
                        };

                        if (types.has.call(context, key)) {
                            temp = temp[key];
                        } else {
                            temp = new Error("could not find " + key + " during translation");
                            return;
                        }
                    });
                }
            } else {
                temp = new Error("could not parse options identifier");
            }

            return temp;
        }

        Object.keys(definition).map(function (key) {
            prop = getObjectValue(value, definition[key]);
            if (prop instanceof Error) {
                result = prop;
                return;
            } else {
                result[key] = prop;
            }
        });

        return result;
    }
    
    return {
        compare: function (o, n) {
            types = this.types;
            return compare(this.value, o, n, this.debug);
        },
        translate: function (o) {
            types = this.types;
            return translate(this.value, o, this.debug);
        }
    };
}());

Object.keys(comparison).map(function (key) {
    "use strict";
    exports[key] = comparison[key];
});
},{}],5:[function(require,module,exports){
/*jslint nomen:true, node:true, white:true */
/*globals */
"use strict";
var modules = [];
var keys = {};

modules.push(require("./objects"));
modules.push(require("./strings"));
modules.push(require("./comparison"));

modules.map(function (module) {
    Object.keys(module).map(function (key) {
        exports[key] = module[key];
        keys[key] = key;
    });
});

exports.keys = keys;
},{"./comparison":4,"./objects":6,"./strings":7}],6:[function(require,module,exports){
/*jslint nomen:true, node:true, white:true */
/*globals console */
"use strict";
var objects = (function () {
    return {
        isNull: function (o) {
            return (typeof o === "undefined") || (typeof o === "object" && o === null);
        },
        isObject: function (o) {
            return (typeof o === "object") && (o !== null);
        },
        isEmptyObject: function (o) {
            return ((typeof o === "object") && (Object.keys(o).length === 0));
        },
        isArray: function (o) {
            return !objects.isNull(o) && o instanceof Array;
        },
        isDate: function (o) {
            return !objects.isNull(o) && o instanceof Date;
        },
        isString: function (o) {
            return !objects.isNull(o) && (o instanceof String || typeof o === "string");
        },
        isNumber: function (o) {
            return !objects.isNull(o) && (typeof o === "number" || o instanceof Number) && !isNaN(o);
        },
        isInt: function (o) {
            return objects.isNumber(o) && o.toString().indexOf(".") === -1;
        },
        isFunction: function (o) {
            return (typeof o === "function") && (o instanceof Function);
        },
        has: function (o) {
            return !objects.isNull(this.value) && (this.value.hasOwnProperty(o) || !objects.isNull(this.value[o]));
        },
        log: function (o) {
            if ((typeof console !== "undefined") && (console.log instanceof Function)) {
                console.log(o);
            }
        }
    };
}());

Object.keys(objects).map(function (key) {
    exports[key] = objects[key];
});
},{}],7:[function(require,module,exports){
/*jslint nomen:true, node:true, white:true */
/*globals */
"use strict";
var strings = (function () {
    return {
        _whitespace: new RegExp(/\s/g),
        isNullOrEmpty: function (o) {
            if (typeof o === "string") {
                return (o.replace(strings._whitespace, "") === "");
            } else {
                return true;
            }
        },
        isNonEmptyString: function (o) {
            return (typeof o === "string") && !strings.isNullOrEmpty(o);
        },
        isNumericString: function (o) {
            return strings.isNonEmptyString(o) && !isNaN(o);
        },
        isDateString: function (o) {
            try {
                return strings.isNonEmptyString(o) && !isNaN(Date.parse(o));
            } catch (dateErr) {
                return false;
            }
        }
    };
}());

Object.keys(strings).map(function (key) {
    if (key !== "_whitespace") {
        exports[key] = strings[key];
    }
});
},{}]},{},[1]);
