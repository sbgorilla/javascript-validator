# License

MIT

# Overview 

Module for single property validation as well as objects which match a defined key/pair definition.

# Installation

```
npm install javascript-validator --save
```

# Public Repository

https://bitbucket.org/sbgorilla/javascript-validator

# JSON

Provides a simple mechanism to check if an object matches a specified object literal definition.

# Translation

Provides the ability to flatten a complex object literal to a collection of only the properties request regardless of depth.

```
//after npm install
var jv = require("javascript-validator"); 

var obj = { 
  user: { 
    name: "ted", 
    address: 
    { 
      zip: 555555 
    } 
  } 
};
var definition = { 
    zip: "user.address.zip"
};

var c = jv(obj).translate(definition).result; 
//result { zip: 555555 }
```

# Types

Internal types have exposed keys and they are: 
```
{ isNull: 'isNull',
  isObject: 'isObject',
  isArray: 'isArray',
  isDate: 'isDate',
  isString: 'isString',
  isNumber: 'isNumber',
  isInt: 'isInt',
  isFunction: 'isFunction',
  has: 'has',
  isNullOrEmpty: 'isNullOrEmpty',
  isNonEmptyString: 'isNonEmptyString',
  isNumericString: 'isNumericString',
  isDateString: 'isDateString' }
```

# Example

Although this has the ability to function chain in most cases it is non needed. 

```
//after npm install
var jv = require("javascript-validator"); 

function foo(a){ 
    if(a && typeof a === "string"){
        return true; 
    }
    else { 
        return false; 
    }
}
console.log(foo("test")); 
//returns true

//rewritten
function foo_foo(a){ 
    return jv(a).isString.result; 
}
console.log(foo_foo("test")); 
//returns true
```

# Chaining

Chaining functions will require the object to be evaluated to be passed to the validator.

```
var jv = require("javascript-validator"); 
var obj, strDate;  

obj = {
    age: 5, 
    height: 5.8,
    weight: 190
};

strDate = "5/2/12"; 

console.log(jv(obj).has("age").has("height").has("weight").result); 
//prints true

console.log(jv("5/2/12").isString.isNonEmptyString.isDateString.result);
//prints true

//NOTE use another to pass value to override current value this can be used to combine multiple conditions on multiple variables
console.log(jv(obj).isObject.has("age").another.isString(strDate).isNonEmptyString.isDateString.result);
//prints true
```

# Single calls

Singles call like 'isString' are available for less complex checking. 

```
var jv = require("javascript-validator"); 
console.log(jv("").isString.result; 
//prints true
console.loog(jv("").isNonEmptyString.result; 
//prints false
```

# Extending the validator
Calling configure and passing an array of extension allow for simple additions of extra validations

```
var extension = {
    key: "dayOfTheWeek",
    validation: function (o) {
        var valid, days = ["mon", "tue", "wed", "thu", "fri"];
        //NOTE: withing this function this.types will equal all the validations configured
            days.map(function (key, i) {
                if (key === days[i]) {
                    valid = true;
                    return;
                }
            });
        return valid;
    },
    //setting true requires validation to be called with parenthesis
    //validator("tue").dayOfTheWeek().result
    hasSet: false
};

validator.configure([extension]);
console.log(validator("tue").dayOfTheWeek.result);
//prints true
```