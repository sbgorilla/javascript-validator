/*jslint nomen:true, white:true */
/*globals require, module, __dirname */
(function(){
    "use strict";
    var pkg = require("./package"),
    browserify = require("./lib/json/grunt-browserify"),
    concatOptions = require("./lib/json/concat-options"),
    uglify = require("./lib/json/grunt-uglify"),
    version;

    version = ["dist/jv-", pkg.version, ".js"].join("");

    browserify.client.dest = version;
    concatOptions["non-ugly"].src.push(version);
    concatOptions["non-ugly"].dest = version.replace(".js", ".combined.js");
    uglify.js.files[version.replace(".js", ".min.js")] = version.replace(".js", ".combined.js");

    module.exports = function (grunt) {
        
        grunt.initConfig({
            pkg: pkg,
            mochaTest: require("./lib/json/grunt-mocha"),
            browserify: browserify,
            uglify: uglify
        });

        require("./lib/json/grunt-tasks").map(grunt.loadNpmTasks);

        grunt.registerTask("default", "", function () {
             grunt.task.run(["mochaTest:test"]);
        });
        
        grunt.registerTask("combine", function(){
             var done = this.async();
            require("./lib/combine").run(concatOptions, ["non-ugly"], __dirname, function(){
                done();
            });
        });
        
        grunt.registerTask("build", ["browserify","combine","uglify:js"]);
    };
}());